# wolvenkit-cli-bin

Autoinstaller for CDPR's WolvenKit for Arch Linux.

## Installation

```bash
makepkg
sudo pacman -U ./wolvenkit-cli-bin-*.tar.zst
```

---

<details><summary>Wrapper update (note to self)</summary><br/>

1. Make version change in [PKGBUILD](https://wiki.archlinux.org/title/PKGBUILD)
2. Remove "sha256sums" field
3. Update sources and write new checksum (`makepkg -g >> PKGBUILD`)
4. Update [.SRCINFO](https://wiki.archlinux.org/title/.SRCINFO) (`makepkg --printsrcinfo > .SRCINFO`)
5. Deploy as per [Installation](#installation)

</details>
